File upload testing in AngularJS-Sailsjs
===============

## Dev Environment Set up

### Requirements
- [Virtual Box](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](http://www.vagrantup.com/downloads)

## Developement Setup
##### Clone the repo:
```
git clone git@bitbucket.org:argha0977/file-upload.git

```
##### Goto project folder
```
cd file-upload
```

##### Initiallize & Provision Vagrant Dev Environment
```
vagrant up
```
Vagrant boots up a virtual box and installs all the required server components:

 - NodeJS
 - MongoDB
 - NGINX
 - Compass (Sass)
 - Grunt
 - Bower
 
 Install node modules
```
npm install
```
## Development
Once the Virtual Box is running through Vagarant
SSH to the box
```
vagrant ssh
```
Goto the project source folder (file-upload) mapped as /vagrant/ in the virtual box

```
cd /vagrant/
```
Install bower modules
```
bower install
```
Starts grunt
```
grunt serve
```
Open your browser at http://localhost:9000 you should see the default home page

As you keep modifying and saving your code in the project folder the auto reload will load the page with latest changes

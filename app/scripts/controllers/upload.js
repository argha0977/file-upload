'use strict';

/**
 * @ngdoc function
 * @name vagrantApp.controller:UploadCtrl
 * @description
 * # UploadCtrl
 * Controller of the vagrantApp
 */
angular.module('vagrantApp')
  .controller('UploadCtrl', function ($scope, $upload) {
     $scope.$watch('myFiles', function() {
    for (var i = 0; i < $scope.myFiles.length; i++) {
      var file = $scope.myFiles[i];
      $scope.upload = $upload.upload({
        url: 'http://localhost:1337/fileupload/upload', // upload.php script, node.js route, or servlet url
        //method: 'POST' or 'PUT',
        //headers: {'Authorization': 'xxx'}, // only for html5
        //withCredentials: true,
        data: {myObj: $scope.myModelObj},
        file: file, // single file or a list of files. list is only for html5
        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
        //fileFormDataName: myFile, // file formData name ('Content-Disposition'), server side request form name
                                    // could be a list of names for multiple files (html5). Default is 'file'
        //formDataAppender: function(formData, key, val){}  // customize how data is added to the formData. 
                                                            // See #40#issuecomment-28612000 for sample code

      }).progress(function(evt) {
        console.log('progress: ' + parseInt(100.0 * evt.loaded / evt.total) + '% file :'+ evt.config.file.name);
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);
      });
      
    }
    

  });
  });

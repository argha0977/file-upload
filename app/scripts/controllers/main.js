'use strict';

/**
 * @ngdoc function
 * @name vagrantApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vagrantApp
 */
angular.module('vagrantApp')
  .controller('MainCtrl', function ($scope, Remoteapi) {
  	$scope.message='';
  	$scope.config={pname:'All',nickname:'A'};
    $scope.getConfig= function(){
      Remoteapi.getConfig().
      success(function(data){
        $scope.message=data.pname+'('+data.nickname+')';
        //$scope.createConfig();
      }).
      error(function(data,status){
        $scope.createConfig();
      });
    };

    $scope.createConfig= function(){
    	Remoteapi.createConfig($scope.config).
    	success(function(data){
    		$scope.message='created';
    	}).
    	error(function(data,status){
        $scope.message='error';
      });
    };

    $scope.getConfig();
  });

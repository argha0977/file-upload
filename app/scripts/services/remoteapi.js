'use strict';

angular.module('vagrantApp')
.factory('Remoteapi', function($http){
	/*var server=SERVER.server;
	var port=SERVER.port;
	var url=''//http://'+server+port+'/';*/

	//Call all backend API using $http service
	return{

		getConfig: function(){
			return $http.get('events/config.json');
		},
		createConfig: function(config){console.log(config);
			return $http.post('events/config.json',config);
		}
	};
});
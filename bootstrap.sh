#!/usr/bin/env bash


sudo apt-get update -qq 
sudo apt-get install -qqy software-properties-common python-software-properties
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update -qq 
sudo apt-get install -qqy nodejs

sudo apt-get install -qqy git

sudo npm install -g grunt-cli

sudo npm install -g bower

sudo npm install -g yo

sudo npm install -g generator-angular
